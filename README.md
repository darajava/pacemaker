### Generating playlist on Spotify

- If making a new genre, add a subfolder into `genres/`
- In that folder, make a file with a list of album titles, with the artist's name on the same line.

```
classic/albums.txt:

Astral Weeks - Van Morrison
Bringing it all Back Home - Bob Dylan
...
```

- In the project root folder, run `split-by-bpm.py`:

```
python3 split-by-bpm.py genres/classic/ kinda
```

The second argument, `strict` defines how strict the BPM matching is. `very` only includes exact BPM matches, `not` also includes them on +-1 lists and `kinda` is a mix between the two, see source for details. Use `very` when you have a large input dataset. This script can be run multiple times and will append to existing BPM lists.

`split-by-bpm.py` finds all the IDs for the tracks in the input list (or all the tracks for each album) and splits them into lists by BPM in the `lists/` folder of the selected genre, like so:

```
    lists/
    ├── 066bpm
    ├── 067bpm
    ├── 068bpm
    ├── 072bpm
    ├── 073bpm
```

- The above step won't rank the lists in order of danceability, and will likely leave duplicates so run the following to sort and uniq the lists:

```
./clean-up-lists.bash genres/classic/lists
```

- To generate Spotify playlists from the BPM lists run:

```
python3 convert-lists-into-playlists.py genres/classic/lists/ Classic [minBPM] [maxBPM]
```

N.B. Do not forget the second arg (`Classic` in this case) which is used for the genre in the playlist's name. (i.e. "Classic tracks @ 108bpm")

This step will result in a JSON file in the genre's folder called `playlists.json` containing the IDs of the generated playlists with associated BPMs. This file can be consumed by the frontend.

### TODOs
- Don't hardcode `client_id` and `client_secret`
- Use argparse to ensure correct params to python scripts
