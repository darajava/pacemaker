import React, { useState, useEffect } from 'react';
import Controls from '../Controls/Controls';
import hiphop from '../lists/hiphop';
import pop from '../lists/pop';
import indie from '../lists/metal';
import house from '../lists/house';
import CSSModules from 'react-css-modules';
import './styles.css';

function MainScreen() {
  const [bpm, setBpm] = useState(72)
  const [animTrigger, setAnimTrigger] = useState()
  const [genre, setGenre] = useState('hip-hop')
  const [list, setList] = useState(hiphop);

  let timeout;
  
  const [changing, setChanging] = useState(false);
  let changingTimeout;

  useEffect(() => {
    console.log(changing)
    clearTimeout(changingTimeout)
    setChanging(true);
    changingTimeout = setTimeout(() => setChanging(false), 800);
  }, [bpm]);

  useEffect(() => {
    switch (genre) {
      case "house":
        setList(house);
        break;
      case "pop":
        setList(pop);
        break;
      case "hip-hop":
        setList(hiphop);
        break;
      case "indie":
        setList(indie);
        break;
    }
  }, [genre]);


  return (
    <div className="main-container">
      <div className="logo-holder">
        <div className="logo">pacemaker</div>
        <div className="tagline">run to the beat</div>
      </div>
      <div style={{color: 'red', animationDuration: changing ? '100s' : ((60 / bpm) + 's') }} className={'pulse ' + genre} />
      <Controls key={1} list={list} genre={genre} onBpmChange={setBpm} onGenreChange={setGenre}/>
    </div>
  );
}
    
export default MainScreen;