import React, { useState, useEffect } from 'react';
import CSSModules from 'react-css-modules';
import styles from './styles.module.css';

function clamp(num, min, max) {
  return num <= min ? min : num >= max ? max : num;
}

function Controls(props) {
  const [bpm, setBpm] = useState(props.list[0].bpm);

  console.log(props.list)
  console.log(bpm)

  let url = props.list.find((e) => e.bpm == bpm);
  if (url) {
    url = url.url;
  } else {
    setBpm(clamp(bpm, props.list[0].bpm, props.list[props.list.length-1].bpm))
  }
  return (
    <div styleName="container">
      <div>
        <div styleName="genre">
          GENRE
        </div>
        <select styleName="select" onChange={(e) => props.onGenreChange(e.target.value)}>
          <option value="hip-hop">hip-hop</option>
          <option value="pop">pop</option>
          <option value="metal">metal</option>
          <option value="house">house</option>
        </select>
      </div>
      <div>
        <span styleName="bpm">{bpm} BPM</span>
        <input
          styleName="track"
          onChange={(e) => {props.onBpmChange(e.target.value); setBpm(e.target.value)}}
          type="range"
          value={bpm}
          min={props.list[0].bpm}
          max={props.list[props.list.length - 1].bpm}
        />
      </div>
      <div>
      <a target="_blank" styleName="go" href={'https://open.spotify.com/playlist/' + url}>
        <div>Play on</div>
        <img src="/spotify.png" />
      </a>
      </div>
    </div>
  );
}
    
export default CSSModules(Controls, styles);
