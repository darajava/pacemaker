echo "before:"
cat $1/* | wc -l

for file in $1/*
do
    if [[ -f $file ]]; then
        sort -ru $file -o $file      
    fi
done

echo "after:"
cat $1/* | wc -l