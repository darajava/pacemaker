import os
import sys
from shutil import copyfile

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# for each file, check its length
# if too short combine contents with next slowest and next fastest bpm
# and write that into a new file
for file in sorted(os.listdir(sys.argv[1])):
  length = file_len(os.path.join(sys.argv[1], file));
  if length < int(sys.argv[2]):
    bpm = int(file.strip('bpm'))
    
    new = [];
    try:
      with open(os.path.join(sys.argv[1], str(bpm+1).zfill(3) + 'bpm')) as list:
        for line in list:
          new.append(line)

      with open(os.path.join(sys.argv[1], str(bpm-1).zfill(3) + 'bpm')) as list:
        for line in list:
          new.append(line)

      with open(os.path.join(sys.argv[1], str(bpm).zfill(3) + 'bpm')) as list:
        for line in list:
          new.append(line)

      with open(os.path.join(sys.argv[1], file + 'new'), 'w') as list:
        for line in new:
          list.write(line)
    except Exception as e:
      print(e)

# take all the new files we wrote and move them back into their original files
for file in sorted(os.listdir(sys.argv[1])):
  if "new" in file:
    copyfile(os.path.join(sys.argv[1], file.replace('new', '')), os.path.join(sys.argv[1], file.replace('new', '') + 'old'))
    copyfile(os.path.join(sys.argv[1], file), os.path.join(sys.argv[1], file.replace('new', '')))