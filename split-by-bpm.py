import spotipy
import os
import sys
import time
import spotipy.oauth2 as oauth2
import json

'''
Parse a list of track names or album names into lists of spotify IDs divided by bpm

Usage:
python3 split-by-bpm.py genredir strictbpm=[very/kinda/not]

where genredir is a directory with newline-separated lists of album names. it
is useful to include the artists name to ensure more accurate search results, i.e:

    ...
    Rakim - When I B On Tha Mic 
    Devin The Dude - What a Job  
    Nas - Affirmative Action  
    Geto Boys - I Tried 
    ...


Output:

A list of files containing spotify IDs in outputdir in the following form:
    
    lists/
    ├── 066bpm
    ├── 067bpm
    ├── 068bpm
    ├── 072bpm
    ├── 073bpm
    ├── 074bpm
    ├── 075bpm
    ├── 076bpm
    ├── 077bpm

Where the number indicates the BPM of the track. These files take the form:

    ...
    06690 4Qxv2L4JICAWLFrBV6QeH8
    09310 0LWkaEyQRkF0XAms8Bg1fC
    07600 6iadTWoIsMQX0leDoADIXk
    07550 0qldEqAtjaDY4Gfl4h8oS7
    ...

Where the first number is indicative of the track's popularity (between 000 and 100), and the
second number is the Spotify ID for the track

'''

if len(sys.argv) < 3:
  print(('Usage: {0} genredir strictbpm'.format(sys.argv[0])))
  exit();

credentials = oauth2.SpotifyClientCredentials(
    client_id='2cd83fa508d04cfd9456ba529464b6ef',
    client_secret='e86c8cecfdf84b6aa0c659bcb8c5d35f')

token = credentials.get_access_token()
spotify = spotipy.Spotify(auth=token)

listdir = sys.argv[1] + '/lists/';
try:
  os.mkdir(listdir)
except:
  print(listdir + ' exists')

def getSpotifyId(searchTerm, type="track"):
  try:
    results = spotify.search(q=searchTerm, type=type)
  except Exception as e:
    print(e);
    print('sleeping')
    time.sleep(10);
    return getSpotifyId(searchTerm, type=type);

  try:
    return results[type + 's']['items'][0]['id'];


  except Exception as e:
    print('    ERR: No search results for ' + searchTerm)
    return None 

def writeToFile(filename, contents):
  if os.path.exists(filename):
    append_write = 'a' 
  else:
    append_write = 'w'
  bpmFile = open(filename, append_write)
  bpmFile.write(contents + '\n')
  bpmFile.close()

def writeSongFeatures(ids):
  if not ids:
    return

  features = spotify.audio_features(ids)

  for feature in features:
    bpm = round(feature['tempo'])
    danceability = str(round(feature['danceability'] * 100)).zfill(3)

    if feature['danceability'] < 0.4:
      continue
    if bpm > 180 or bpm < 75:
      continue
    if feature['time_signature'] != 4:
      continue

    writeToFile(listdir + str(bpm).zfill(3) + 'bpm', danceability + ' ' + feature['id'])

    if sys.argv[2] == 'kinda':
      # If we are on a 'cusp' bpm (e.g 118.6 bpm) then also include on lower, same for higher
      closelowerbpm = round(feature['tempo'] - 0.15)
      if closelowerbpm != bpm:
        writeToFile(listdir + str(closelowerbpm).zfill(3) + 'bpm', danceability + ' ' + feature['id'])

      closehigherbpm = round(feature['tempo'] + 0.15)
      if closehigherbpm != bpm:
        writeToFile(listdir + str(closehigherbpm).zfill(3) + 'bpm', danceability + ' ' + feature['id'])
    elif sys.argv[2] == 'not':
      writeToFile(listdir + str(bpm+1).zfill(3) + 'bpm', danceability + ' ' + feature['id'])
      writeToFile(listdir + str(bpm-1).zfill(3) + 'bpm', danceability + ' ' + feature['id'])


def getAlbum(searchTerm):
  print("Getting tracks for " + searchTerm.strip().title())
  albumId = getSpotifyId(searchTerm, type="album")
  if albumId:
    tracks = spotify.album_tracks(albumId)['items']
  else:
    return;

  trackIds = []
  for track in tracks:
    trackIds.append(track['id'])

  writeSongFeatures(trackIds)

for file in sorted(os.listdir(sys.argv[1])):
  if not os.path.isdir(os.path.join(sys.argv[1], file)) and not file.endswith(".json"):
    print(os.path.join(sys.argv[1], file))
    with open(os.path.join(sys.argv[1], file)) as songs:
      for line in songs:
        getAlbum(line)
          


