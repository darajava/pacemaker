import spotipy
import os
import sys
import time
import spotipy.oauth2 as oauth2
import json
import spotipy.util as util

'''
usage: python3 convert-lists-into-playlists.py foldername genre-name [minBPM] [maxBPM]
'''


if len(sys.argv) < 3:
  print(('Usage: {0} foldername genre-name [minBPM] [maxBPM]'.format(sys.argv[0])))
  exit()

scope = 'playlist-modify-public, playlist-modify-private'
token = util.prompt_for_user_token(username="1194469055", scope=scope, client_id='2d4c831ebb7d486bb86ae276e1cc5e0b',
        client_secret='f787e783a2664130a6669a656dbf0559', redirect_uri='http://localhost:1500/callback')

if token:
    sp = spotipy.Spotify(auth=token)
    sp.trace = False
    # sp.user_playlist_create("1194469055", "test", public=True)['id'],
else:
    print("Can't get token")


def writeToFile(filename, contents):
  append_write = 'w'
  bpmFile = open(filename, append_write)
  bpmFile.write(contents + '\n')
  bpmFile.close()

def addToPlaylist(playlist_id, track_ids):
    try:
        sp.user_playlist_add_tracks("1194469055", playlist_id, track_ids)
    except Exception as e:
        time.sleep(1)
        print(e)
        addToPlaylist(playlist_id, track_ids)

jsonList = []

def processBpmFile(filename):
    count = 0
    bpm = int(filename.strip('bpm'));

    try:
        playlistId = sp.user_playlist_create("1194469055", sys.argv[2] + " tracks @ " + str(bpm) + " BPM", public=False)['id']
    except Exception as e:
        time.sleep(1)
        print(e)
        processBpmFile(filename)
        return;
    # exit();
    with open(sys.argv[1] + '/' + filename) as playlist:
        track_ids = []
        for track in playlist:
            if count < 40:
                count = count + 1
                track_ids.append(track.split()[1])
        addToPlaylist(playlistId, track_ids)

    trackJson = {"url": playlistId, "bpm": bpm, "trackCount": len(track_ids)};
    print(trackJson)
    jsonList.append(trackJson)
    writeToFile(sys.argv[1] + '/../playlists.json', json.dumps(jsonList, indent=2))

for filename in sorted(os.listdir(sys.argv[1])):
    if not "old" in filename and not "new" in filename:
        processBpmFile(filename)


writeToFile(sys.argv[1] + '/../playlists.json', json.dumps(jsonList, indent=2))

